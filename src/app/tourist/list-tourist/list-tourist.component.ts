import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddTouristComponent } from 'src/app/tourist/add-tourist/add-tourist.component';
import { TouristCRUDService } from 'src/app/Services/Tourist/tourist-crud.service';

@Component({
  selector: 'app-list-tourist',
  templateUrl: './list-tourist.component.html',
})
export class ListTouristComponent implements OnInit {

  listTouristData: MatTableDataSource<any> = null;
  displayedColumns: string[] = [
    'name',
    'lastName',
    'birdDate',
    'iDNumber',
    'iDType',
    'travelFrecuency',
    'travelBudget',
    'destiny',
    'creditCard',
    'actions'
  ];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private touristService: TouristCRUDService
  ) { }

  ngOnInit(): void {
    this.touristService.initializeDataCities().subscribe({
      error: () => this.listTouristData = new MatTableDataSource(),
      complete: () => {
        this.TableSources();
      }
    });
  }

  TableSources(): void {
    this.touristService.getTourist().subscribe(
      res => {
        const tourist: any[] = res;
        tourist.forEach(data => {
          // Obtiene el nombre de cada ciudad y remplaza su valor en la tabla
          this.touristService.dataCities.forEach(city => {
            if (data.destiny === city.id) {
              data.destiny = city.name;
            }
          });
          data.creditCard = (data.creditCard === 0) ? 'Si' : 'No';
        });

        this.listTouristData = new MatTableDataSource(tourist);
        this.listTouristData.sort = this.sort;
        this.listTouristData.paginator = this.paginator;
      }
    );
  }

  onAdd(): void {
    this.touristService.actionType = 'Add';
    this.touristService.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddTouristComponent, dialogConfig);
  }

  onEdit(row: any): void {
    this.touristService.actionType = 'Update';
    this.touristService.editTourist(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddTouristComponent, dialogConfig);
  }

  onRemove(row: any): void {
    if (confirm('Quieres eliminar este valor?')) {
      this.touristService.removeTourist(row.id);
    }
  }
}
