import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTouristComponent } from './list-tourist.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { TouristCRUDService } from 'src/app/Services/Tourist/tourist-crud.service';

describe('ListTouristComponent', () => {
  let component: ListTouristComponent;
  let fixture: ComponentFixture<ListTouristComponent>;
  let httpMock: HttpClientTestingModule;
  let matSnackBarModule: MatSnackBarModule;
  let matDialogModule: MatDialogModule;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListTouristComponent,
      ],
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        MatDialogModule
      ],
      providers: [

      ]
    })
    .compileComponents();
    httpMock = TestBed.inject(HttpClientTestingModule);
  }));

  // beforeEach(() => {
  //   matSnackBarModule = TestBed.inject(MatSnackBarModule);
  //   matDialogModule = TestBed.inject(MatDialogModule);
  //   fixture = TestBed.createComponent(ListTouristComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  // it('ListTourist', () => {
  //   expect(true).toBeTruthy();
  // });
});
