import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/Services/notification.service';
import { MatDialogRef } from '@angular/material/dialog';
import { TouristCRUDService } from 'src/app/Services/Tourist/tourist-crud.service';

@Component({
  selector: 'app-add-tourist',
  templateUrl: './add-tourist.component.html',
})
export class AddTouristComponent implements OnInit {

  constructor(
    private notificationService: NotificationService,
    public touristService: TouristCRUDService,
    public dialogRef: MatDialogRef<AddTouristComponent>
  ) { }

  ngOnInit(): void { }

  sendDataTourist(): void {
    if (this.touristService.formGroup.valid) {
      this.touristService.sendTourist(this.touristService.formGroup.value);
      this.notificationService.warn(':: Enviando... ');
      this.touristService.clearForm();
      this.dialogRef.close();
    }
  }
}
