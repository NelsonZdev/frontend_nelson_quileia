import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTouristComponent } from './add-tourist.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA , MatDialog, MatDialogModule} from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';

describe('AddTouristComponent', () => {
  let component: AddTouristComponent;
  let fixture: ComponentFixture<AddTouristComponent>;
  let httpMock: HttpClientTestingModule;
  let httpClient: HttpClient;
  let httpHandler: HttpHandler;
  let matSnackBarModule: MatSnackBarModule;
  let formBuilder: FormBuilder;
  let validators: Validators;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AddTouristComponent,
      ],
      providers: [
        HttpClientTestingModule,
        HttpClient,
        HttpHandler,
        FormBuilder,
        Validators,
        {
          provider: MatDialogRef,
          useValue: {}
        },
        {
          provider: MAT_DIALOG_DATA,
          useValue: {}
        }
      ],
      schemas: [

      ],
      imports: [
        MatSnackBarModule,
        MatDialogModule
      ]
    })
      .compileComponents();
    httpMock = TestBed.inject(HttpClientTestingModule);
    httpClient = TestBed.inject(HttpClient);
    httpHandler = TestBed.inject(HttpHandler);
    formBuilder = TestBed.inject(FormBuilder);
    validators = TestBed.inject(Validators);
  }));

  // beforeEach(() => {
  //   matSnackBarModule = TestBed.inject(MatSnackBarModule);
  //   fixture = TestBed.createComponent(AddTouristComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  // it('should create', () => {
  //   expect(true).toBeTruthy();
  // });
});
