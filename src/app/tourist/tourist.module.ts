import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListTouristComponent } from './list-tourist/list-tourist.component';
import { AddTouristComponent } from './add-tourist/add-tourist.component';

@NgModule({
  declarations: [
    ListTouristComponent,
    AddTouristComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ]
})
export class TouristModule { }
