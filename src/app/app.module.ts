import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataService } from './Services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { TouristCRUDService } from 'src/app/Services/Tourist/tourist-crud.service';
import { CityCRUDService } from 'src/app/Services/City/city-crud.service';
import { BookTripCRUDService } from 'src/app/Services/Book-trip/book-trip-crud.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [
    DataService,
    TouristCRUDService,
    CityCRUDService,
    BookTripCRUDService,
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
