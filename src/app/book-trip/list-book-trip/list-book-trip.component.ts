import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataService } from 'src/app/Services/data.service';
import { BookTripCRUDService } from '../../Services/Book-trip/book-trip-crud.service';
import { NewBookTripComponent } from '../new-book-trip/new-book-trip.component';

@Component({
  selector: 'app-list-book-trip',
  templateUrl: './list-book-trip.component.html',
})
export class ListBookTripComponent implements OnInit {

  listBookTripData: MatTableDataSource<any> = null;
  displayedColumns: string[] = [
    'idCity',
    'idTourist',
    'travelDate',
    'registrationDate',
  ];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private dialog: MatDialog,
    private bookTripService: BookTripCRUDService
  ) { }

  ngOnInit(): void {
    // Obtiene los datos de la ciudad y turista desde la base de datos y los muestra en la pagina
    // Ciudad
    this.bookTripService.initializeDataCities().subscribe({
      error: () => this.listBookTripData = new MatTableDataSource(), // Tabla vacia
      complete: () => {
        // Turista
        this.bookTripService.initializeDataTourist().subscribe({
          error: () => this.listBookTripData = new MatTableDataSource(),
          complete: () => {
            this.TableSources();
          }
        });
      }
    });
  }

  // Obtiene los nombres de las ciudades y de los turistas en base a la id
  TableSources(): void {
    this.bookTripService.getBookTrips().subscribe(
      res => {
        const bookTrip: any[] = res;
        bookTrip.forEach(
          data => {
            // Ciudad
            this.bookTripService.dataCities.forEach(
              city => {
                if (data.idCity === city.id) {
                  data.idCity = city.name;
                }
              }
            );
            // Turista
            this.bookTripService.dataTourist.forEach(
              tourist => {
                if (data.idTourist === tourist.id) {
                  data.idTourist = tourist.name;
                }
              }
            );
          }
        );

        this.listBookTripData = new MatTableDataSource(bookTrip);
        this.listBookTripData.sort = this.sort;
        this.listBookTripData.paginator = this.paginator;
      }
    );
  }

  onAdd(): void {
    this.bookTripService.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(NewBookTripComponent, dialogConfig);
  }
}
