import { Component, Input, OnInit } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { debounceTime, map } from 'rxjs/operators';
import { BookTripCRUDService } from 'src/app/Services/Book-trip/book-trip-crud.service';
import { NotificationService } from 'src/app/Services/notification.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-new-book-trip',
  templateUrl: './new-book-trip.component.html',
})
export class NewBookTripComponent implements OnInit {

  bookTripArray: any[];
  travelCount = 0;

  constructor(
    private dataService: DataService,
    private notificationService: NotificationService,
    public bookTripService: BookTripCRUDService,
    public dialogRef: MatDialogRef<NewBookTripComponent>
  ) { }

  ngOnInit(): void {

    // Save booktrip Data into a array.
    this.bookTripService.getBookTrips().subscribe(
      res => {
        this.bookTripArray = res;
      }
    );

    let idForm = '';
    let dateForm = '';

    // Peticiones de viaje por dia
    // Envia una peticion con la identifiacion de la ciudad
    this.bookTripService.formGroup.get('idCity')
      .valueChanges.pipe( // Espera al cambio en el formulario / obtiene el "input" que fue modificado
        debounceTime(300)
      ).subscribe((data) => {
        idForm = data;
        // Envio de datos al servicio para consultar si se puede enviar el nuevo registro / Las solicitudes imcompletas se ignoran
        this.travelCount = this.DataIsReady(idForm, dateForm);
      });
    // Es el mismo troso que arriba pero con difernete "input"
    // Envia una peticion con la fecha del posible viaje
    this.bookTripService.formGroup.get('travelDate').valueChanges.pipe(
      debounceTime(300)
    ).subscribe((data) => {
      dateForm = data;
      this.travelCount = this.DataIsReady(idForm, dateForm);
    });
  }

  DataIsReady(id: string, dateTravel: string): number {
    let cities = 0;
    if (id !== '' && dateTravel !== '') {
      this.bookTripArray.map(
        data => {
          if (data.idCity == id && data.travelDate == dateTravel) {
            cities++;
          }
        });
      console.log(cities);
      return cities;
    } else {
      return cities;
    }
  }

  SendDataBookTrip(): void {
    if (this.travelCount < 5 && this.bookTripService.formGroup.status !== 'INVALID') { // No mas de 5 turistas por dia y validacion
      this.bookTripService.sendBookTrip(this.bookTripService.formGroup.value, 'POST');
      this.notificationService.success(':: Envio Exitoso !!!');
      this.bookTripService.clearForm();
      this.dialogRef.close();
    }
  }
}
