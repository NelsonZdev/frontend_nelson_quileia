import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListBookTripComponent } from './list-book-trip/list-book-trip.component';
import { NewBookTripComponent } from './new-book-trip/new-book-trip.component';

@NgModule({
  declarations: [
    ListBookTripComponent,
    NewBookTripComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ]
})
export class BookTripModule { }
