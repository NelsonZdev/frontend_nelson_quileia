import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { NotificationService } from 'src/app/Services/notification.service';
import { MatDialogRef } from '@angular/material/dialog';
import { CityCRUDService } from 'src/app/Services/City/city-crud.service';


@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  providers: [DataService]
})
export class AddCityComponent implements OnInit {

  constructor(
    private notificationService: NotificationService,
    public dialogRef: MatDialogRef<AddCityComponent>,
    public cityService: CityCRUDService
  ) { }

  ngOnInit(): void { }

  sendDataCity(): void {
    if (this.cityService.formGroup.valid) {
      this.cityService.sendCity(this.cityService.formGroup.value);
      this.notificationService.success(':: Envio Exitoso !!!');
      this.cityService.clearForm();
      this.dialogRef.close();
    }
  }
}
