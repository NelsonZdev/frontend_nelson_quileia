import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListCityComponent } from './list-city/list-city.component';
import { AddCityComponent } from './add-city/add-city.component';

@NgModule({
  declarations: [
    ListCityComponent,
    AddCityComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ]
})
export class CityModule { }
