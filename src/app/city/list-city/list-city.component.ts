import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataService } from 'src/app/Services/data.service';
import { CityCRUDService } from '../../Services/City/city-crud.service';
import { AddCityComponent } from '../add-city/add-city.component';

@Component({
  selector: 'app-list-city',
  templateUrl: './list-city.component.html',
  providers: [DataService]
})
export class ListCityComponent implements OnInit {

  listCityData: MatTableDataSource<any> = null;
  displayedColumns: string[] = [
    'name',
    'population',
    'recomendedPlace',
    'hotel',
    'actions'
  ];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dataService: DataService,
    private cityService: CityCRUDService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.cityService.getCities().subscribe(
      res => {
        this.listCityData = new MatTableDataSource(res);
        this.listCityData.sort = this.sort;
        this.listCityData.paginator = this.paginator;
      },
      err => {
        this.listCityData = new MatTableDataSource();
        console.log(err);
      }
    );
  }

  onAdd(): void {
    this.cityService.actionType = 'Add';
    this.cityService.initializeFormGroup();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddCityComponent, dialogConfig);
  }

  onEdit(row: any): void {
    this.cityService.actionType = 'Update';
    this.cityService.editCity(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '60%';
    this.dialog.open(AddCityComponent, dialogConfig);
  }

  onRemove(row: any): void {
    if (confirm('Quieres eliminar este valor?')) {
      this.cityService.removeCity(row.id);
    }
  }
}
