import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { MaterialModule } from './material/material.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('AppComponent', () => {

  let httpMock: HttpClientTestingModule;
  let httpClient: HttpClient;
  let materialModule: MaterialModule;

  // beforeEach(async(() => {
  //   TestBed.configureTestingModule({
  //     imports: [
  //       RouterTestingModule,
  //       HttpClientTestingModule,
  //       MaterialModule
  //     ],
  //     declarations: [
  //       AppComponent,
  //     ],
  //   }).compileComponents();
  //   materialModule = TestBed.inject(MaterialModule);
  //   httpMock = TestBed.inject(HttpClientTestingModule);
  //   httpClient = TestBed.inject(HttpClient);
  // }));


  // it('should create the app', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   expect(app).toBeTruthy();
  // });

  // it(`should have as title 'ExamenWebQUILEIA'`, () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   expect(app.title).toEqual('ExamenWebQUILEIA');
  // });

  // it('should render title', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.nativeElement;
  //   expect(compiled.querySelector('.content span').textContent).toContain('ExamenWebQUILEIA app is running!');
  // });
});
