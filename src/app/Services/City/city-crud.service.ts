import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from '../data.service';
import { NotificationService } from '../notification.service';

@Injectable({
  providedIn: 'root'
})
export class CityCRUDService {

  actionType: string;
  httpAction: any;
  formGroup = this.formBuider.group({
    name: ['', Validators.required],
    population: ['', Validators.required],
    recomendedPlace: ['', Validators.required],
    hotel: ['', Validators.required],
    id: ['']
  });

  constructor(
    private dataService: DataService,
    private formBuider: FormBuilder,
    private notificationService: NotificationService
  ) {
    this.httpAction = dataService.httpAction;
  }

  initializeFormGroup(): void {
    this.formGroup.setValue({
      name: '',
      population: '',
      recomendedPlace: '',
      hotel: '',
      id: ''
    });
  }

  getCities(): Observable<any[]> {
    return this.dataService.GetData(this.httpAction.city).pipe(
      map((res) => {
        return res;
      })
    );
  }

  editCity(data: any): void {
    this.formGroup.setValue({
      id: data.id,
      name: data.name,
      population: data.population,
      recomendedPlace: data.recomendedPlace,
      hotel: data.hotel
    });
  }

  sendCity(formValue: any): void {
    if (this.actionType === 'Update') {
      this.dataService.SendData(this.httpAction.city, formValue, 'PUT').subscribe({
        complete: () => {
          this.dataService.ReLoadModule(this.dataService.usermodules.city);
          console.log('Envio exitoso!');
        }
      });
    } else if (this.actionType === 'Add') {
      this.dataService.SendData(this.httpAction.city, formValue, 'POST').subscribe({
        complete: () => {
          this.dataService.ReLoadModule(this.dataService.usermodules.city);
          console.log('Envio exitoso!');
        }
      });
    }
  }

  removeCity(id: number): void {
    this.dataService.RemoveData(this.httpAction.city, id).subscribe({
      complete: () => {
        this.dataService.ReLoadModule(this.dataService.usermodules.city);
        console.log('Envio exitoso!');
      }
    });
    this.notificationService.warn(':: Se ha eliminado un elemento !');
  }

  clearForm(): void{
    this.formGroup.reset();
    this.initializeFormGroup();
  }
}
