import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  httpBase: string;
  httpHeaders: HttpHeaders;
  dataTest: object;
  public httpAction = {
    tourist: '/tourist/',
    city: '/city/',
    bookTrip: '/bookTrip/'
  };
  public usermodules = {
    tourist: 0,
    city: 1,
    bookTrip: 2,
    default: -1
  };
  userModule: number;

  constructor(private http: HttpClient, public notificationService: NotificationService) {
    this.httpBase = 'http://192.168.0.100:8180';
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*'
    });
  }

  ReLoadModule(module: number, timerMS: number = 100, eliminar: boolean = false): void {
    // Si es mayor a 250 milisegundos
    if (timerMS > 250 && !eliminar) {
      // Espere una 8va parte del tiempo cuando termine la espera
      setTimeout(() => {
        this.userModule = this.usermodules.default; // Cambie modulo a defecto
        setTimeout(() => {
          this.userModule = module; // Cambie modulo segun el seleccionado
          this.notificationService.success(':: Enviado !!! ');
        }, timerMS / 8);
      }, timerMS);
    } else if (eliminar) {
      this.userModule = this.usermodules.default;
      setTimeout(() => {
        this.userModule = module;
        this.notificationService.success(':: Eliminado !!! ');
      }, timerMS);
    }
    else {
      this.userModule = this.usermodules.default;
      setTimeout(() => {
        this.userModule = module;
        this.notificationService.success(':: Enviado !!! ');
      }, timerMS);
    }
  }

  GetData(httpAction: string): Observable<any[]> {
    return this.http.get<any[]>(this.httpBase + httpAction, { headers: this.httpHeaders });
  }

  GetSingleData(httpAction: string, id: number): Observable<any> {
    return this.http.get<any>(this.httpBase + httpAction + id, { headers: this.httpHeaders });
  }

  QueryCityAndTravel(cityID: string, travelDate: string): Observable<number> {

    return this.http.get<number>(this.httpBase + 'cityID=' + cityID + '&travelDate=' + travelDate);
  }

  SendData(httpAction: string, data: any, request: string): Observable<object> {
    switch (request) {
      case 'POST':
        return this.http.post(this.httpBase + httpAction, data, { responseType: 'json',  headers: this.httpHeaders });
      case 'PUT':
        return this.http.put(this.httpBase + httpAction + data.id, data, { responseType: 'json',  headers: this.httpHeaders });
      default:
        break;
    }
  }

  RemoveData(httpAction: string, id: number): Observable<object> {
    return this.http.delete(this.httpBase + httpAction + id, { headers: this.httpHeaders });
  }
}
