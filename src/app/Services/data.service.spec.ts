import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NotificationService } from 'src/app/Services/notification.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Overlay } from '@angular/cdk/overlay';
import { map } from 'rxjs/operators';


class MockNotificationService {
  success(): string {
    return 'OK';
  }
}

describe('DataService', () => {
  const URL = 'http://192.168.0.100:8180';
  const OPTION = '/tourist';
  let notificationService: NotificationService;
  let matSnackBar: MatSnackBar;
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;
  let dataService: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        DataService,
        MatSnackBar,
        Overlay,
        NotificationService
      ]
    }).compileComponents();
    notificationService = TestBed.inject(NotificationService);
    matSnackBar = TestBed.inject(MatSnackBar);
    httpTestingController = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    dataService = TestBed.inject(DataService);
  });

  afterEach(() => {
    httpTestingController.verify(); // Verifies that no requests are outstanding.
  });

  it('get Tourist', async(() => {
    const MOK_TOURIST = {
      id: '999',
      name: 'Demo',
      lastName: 'Demo',
      birdDate: '2000-01-01',
      iDNumber: '0001',
      iDType: 'CC',
      travelFrecuency: '0',
      travelBudget: '0',
      destiny: '1',
      creditCard: '0'
    };
    // Request to BackEnd
    // http.post(URL + OPTION, MOK_TOURIST, { responseType: 'json' });
    const req = httpTestingController.expectOne({ method: 'GET', url: URL + OPTION });
    req.flush({});
  }));
});
