import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CityCRUDService } from '../City/city-crud.service';
import { DataService } from '../data.service';
import { TouristCRUDService } from '../Tourist/tourist-crud.service';

@Injectable({
  providedIn: 'root'
})
export class BookTripCRUDService {

  httpAction: any;
  dataCities: any[];
  dataTourist: any[];

  formGroup = this.formBuider.group({
    idCity: ['', Validators.required],
    idTourist: ['', Validators.required],
    travelDate: ['', Validators.required],
  });

  constructor(
    private dataService: DataService,
    private formBuider: FormBuilder,
    private touristService: TouristCRUDService,
    private cityService: CityCRUDService,
  ) {
    this.httpAction = dataService.httpAction;
  }

  initializeDataTourist(): Observable<any> {
    return this.touristService.getTourist().pipe(
      map((res => {
        this.dataTourist = res;
      }))
    );
  }

  initializeDataCities(): Observable<any> {
    return this.cityService.getCities().pipe(
      map((res => {
        this.dataCities = res;
      }))
    );
  }

  initializeFormGroup(): void {
    this.formGroup.setValue({
      idCity: '',
      idTourist: '',
      travelDate: '',
    });
  }

  getBookTrips(): Observable<any[]> {
    return this.dataService.GetData(this.httpAction.bookTrip).pipe(
      map((res) => {
        return res;
      })
    );
  }

  sendBookTrip(formValue: any, request: string): void {
    this.dataService.SendData(this.httpAction.bookTrip, formValue, request).subscribe({
      complete: () => {
        this.dataService.ReLoadModule(this.dataService.usermodules.bookTrip);
        console.log('Envio exitoso!');
      }
    });
  }

  clearForm(): void {
    this.formGroup.reset();
    this.initializeFormGroup();
  }
}
