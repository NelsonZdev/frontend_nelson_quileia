import { TestBed } from '@angular/core/testing';

import { TouristCRUDService } from './tourist-crud.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { MaterialModule } from 'src/app/material/material.module';
import { FormBuilder, Validators } from '@angular/forms';
import { CityCRUDService } from '../City/city-crud.service';
import { DataService } from '../data.service';
import { NotificationService } from '../notification.service';

describe('TouristCRUDService', () => {
  let service: TouristCRUDService;
  let httpMock: HttpClientTestingModule;
  let httpClient: HttpClient;
  let materialModule: MaterialModule;
  let formBuilder: FormBuilder;
  let validators: Validators;
  let dataService: DataService;
  let cityService: CityCRUDService;
  let notificationService: NotificationService;

  // beforeEach(() => {
  //   TestBed.configureTestingModule({
  //     imports: [
  //       HttpClientTestingModule,
  //     ],
  //     providers: [
  //       TouristCRUDService,
  //       MaterialModule,
  //       FormBuilder,
  //       Validators
  //     ]
  //   });
  //   materialModule = TestBed.inject(MaterialModule);
  //   httpMock = TestBed.inject(HttpClientTestingModule);
  //   httpClient = TestBed.inject(HttpClient);
  //   formBuilder = TestBed.inject(FormBuilder);
  //   validators = TestBed.inject(Validators);

  //   dataService = TestBed.inject(DataService);
  //   cityService = TestBed.inject(CityCRUDService);
  //   notificationService = TestBed.inject(NotificationService);

  //   service = new TouristCRUDService(dataService, cityService, formBuilder, notificationService);
  // });


  // it('Get Tourist', () => {
  //   expect(true).toBeTruthy();
  // });
});
