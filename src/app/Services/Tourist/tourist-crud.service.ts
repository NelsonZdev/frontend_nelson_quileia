import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CityCRUDService } from '../City/city-crud.service';
import { DataService } from '../data.service';
import { NotificationService } from '../notification.service';

@Injectable({
  providedIn: 'root'
})
export class TouristCRUDService {

  dataCities: any[];
  actionType: string; // Identifica la accion del usuario
  httpAction: any;
  formGroup = this.formBuilder.group({
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    birdDate: ['', Validators.required],
    iDNumber: ['', Validators.required],
    iDType: ['', Validators.required],
    travelFrecuency: ['', Validators.required],
    travelBudget: ['', Validators.required],
    destiny: ['', Validators.required],
    creditCard: ['1', Validators.required],
    id: null,
  });

  constructor(
    private dataService: DataService,
    private cityService: CityCRUDService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService
  ) {
    this.httpAction = dataService.httpAction;
    cityService.getCities().subscribe(
      res => this.dataCities = res
    );
  }

  initializeDataCities(): Observable<any> {
    return this.cityService.getCities().pipe(
      map((res => {
        this.dataCities = res;
      }))
    );
  }

  initializeFormGroup(): void {
    this.formGroup.setValue({
      id: null,
      name: '',
      lastName: '',
      birdDate: '',
      iDNumber: '',
      iDType: '',
      travelFrecuency: '',
      travelBudget: '',
      destiny: '',
      creditCard: '1',
    });
  }

  getTourist(): Observable<any[]> {
    return this.dataService.GetData(this.httpAction.tourist).pipe(
      map((res) => {
        return res;
      })
    );
  }

  editTourist(data: any): void {
    // Busca el nombre de la ciudad dentro del "array de ciudades" y obtiene solo 1 valor
    const destiny: any = this.dataCities.find(res => res.name === data.destiny);
    this.formGroup.setValue({
      id: data.id,
      name: data.name,
      lastName: data.lastName,
      birdDate: data.birdDate,
      iDNumber: data.iDNumber,
      iDType: data.iDType,
      travelFrecuency: data.travelFrecuency,
      travelBudget: data.travelBudget,
      destiny: ''.replace('', destiny.id),
      creditCard: (data.creditCard === 'Si') ? '0' : '1',
    });
  }

  sendTourist(formValue: any): void {
    if (this.actionType === 'Update') {
      this.dataService.SendData(this.httpAction.tourist, formValue, 'PUT').subscribe({
        complete: () => {
          this.dataService.ReLoadModule(this.dataService.usermodules.tourist);
          console.log('Envio exitoso!');
        }
      });
    } else if (this.actionType === 'Add') {
      this.dataService.SendData(this.httpAction.tourist, formValue, 'POST').subscribe({
        complete: () => {
          this.dataService.ReLoadModule(this.dataService.usermodules.tourist, 500);
          console.log('Envio exitoso!');
        }
      });
    }
  }

  removeTourist(id: number): void {
    this.notificationService.warn(':: Eliminando elemento...');
    this.dataService.RemoveData(this.httpAction.tourist, id).subscribe({
      complete: () => {
        this.dataService.ReLoadModule(this.dataService.usermodules.tourist, 100, true);
        console.log('Envio exitoso!');
      }
    });
  }

  clearForm(): void {
    this.formGroup.reset();
    this.initializeFormGroup();
  }
}
