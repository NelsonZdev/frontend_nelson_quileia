import { Component, Type } from '@angular/core';
import { DataService } from './Services/data.service';
import { ListTouristComponent } from './tourist/list-tourist/list-tourist.component';
import { ListCityComponent } from './city/list-city/list-city.component';
import { ListBookTripComponent } from './book-trip/list-book-trip/list-book-trip.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'ExamenWebQUILEIA';
  touristComponent: Promise<Type<ListTouristComponent>>;
  cityComponent: Promise<Type<ListCityComponent>>;
  bookTripComponent: Promise<Type<ListBookTripComponent>>;

  constructor(
    public dataService: DataService,
  ) { }

  async GetTourist(): Promise<any> {
    const { TouristModule } = await import('./tourist/tourist.module');

    if (!this.touristComponent) {
      this.touristComponent = import('./tourist/list-tourist/list-tourist.component')
        .then(({ ListTouristComponent }) => ListTouristComponent);
    }

    this.dataService.userModule = 0;
    return false;
  }
  async GetCity(): Promise<any> {
    const { CityModule } = await import('./city/city.module');

    if (!this.cityComponent) {
      this.cityComponent = import('./city/list-city/list-city.component')
        .then(({ ListCityComponent }) => ListCityComponent);
    }

    this.dataService.userModule = 1;
    return false;
  }
  async GetBookTrip(): Promise<any> {
    const { BookTripModule } = await import('./book-trip/book-trip.module');

    if (!this.bookTripComponent) {
      this.bookTripComponent = import('./book-trip/list-book-trip/list-book-trip.component')
        .then(({ ListBookTripComponent }) => ListBookTripComponent);
    }

    this.dataService.userModule = 2;
    return false;
  }
}
