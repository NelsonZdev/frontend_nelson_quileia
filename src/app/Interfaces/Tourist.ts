export interface Tourist {
    'ID': number;
    'name': string;
    'lastName': string;
    'birdDate': Date;
    'iDNumber': string;
    'iDType': string;
    'travelFrecuency': number;
    'travelBudget': number;
    'destiny': number;
    'creditCard': number;
}
