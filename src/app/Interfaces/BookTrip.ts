export interface BookTrip {
    'id': number;
    'idCity': number;
    'idTourist': number;
    'dateTravel': Date;
    'history': string;
}
