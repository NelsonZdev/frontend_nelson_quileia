export interface City {
    'id': number;
    'name': string;
    'population': number;
    'recomendedPlace': string;
    'hotel': string;
}
